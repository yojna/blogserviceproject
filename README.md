Website URL: https://twhyderabad.github.io/demo_site/index.html
Selenium 2.53.0 is used to test the application.
This project containg login into system, create a blog, read and verify blog contents, logout system.

This is a maven project
To run this project...
Download and install Maven from https://maven.apache.org/
Set Environment Path

	1. export M2_HOME="{base_dir}/apache-maven-3.3.3" 
	2. export M2="$M2_HOME/bin"
	3. export PATH=$M2:$PATH
	4. mvn -v //To check maven is installed or not!
	5. Go to repository
	6. mvn clean
	7. mvn package // it will generate class files in target folder and it will automatically run the Test file.

Test script which is available on path xt-solution/src/test/java/in/yojnajain/app
Class name is BlogTest.java


Author
Yojna Jain
Email: yojna1992@gmail.com
M. 091 9154477921, 92282822439

