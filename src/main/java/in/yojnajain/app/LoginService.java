package in.yojnajain.app;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginService {

	public int login(WebDriver driver, String userName, String password) {
		System.out.println("---- Login Test Case ----");
		driver.findElement(By.id("userName")).sendKeys(userName);
		driver.findElement(By.id("password")).sendKeys(password);
		driver.findElement(By.tagName("button")).click();
		WebDriverWait wait = new WebDriverWait(driver, 10);

		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.id("name")));
		} catch (TimeoutException e) {
			e.printStackTrace();
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}

		if (driver.findElement(By.id("name")) != null) {
			if (driver.findElement(By.id("logout")) != null) {
				System.out
						.println("SUCCESS: Login is successfull. Blog form is visible.");
			}
		} else {
			String errorMessage = driver.findElement(By.id("errorMessage"))
					.getAttribute("style");
			if (errorMessage.contains("display: block;")) {
				System.out
						.println("ERROR: Login is not successfull. Showing error wrong username and password.");
			} else if (driver.findElement(By.id("logout")) != null) {
				System.out
						.println("ERROR: Login is successfull. But Form is not visible.");
			} else {
				System.out
						.println("ERROR: Login is not successfull. Enter proper credentials to login");
			}
		}
		return 0;
	}

	public void logout(WebDriver driver) {
		System.out.println("---- Logout Test Case ----");
		if (driver.findElement(By.id("logout")) != null) {
			driver.findElement(By.id("logout")).click();
			if (driver.getCurrentUrl().equals(
					"https://twhyderabad.github.io/demo_site/index.html")) {
				System.out.println("Logout successfull");
			} else {
				System.out.println("Logout not successfull");
			}
		}
	}
}
